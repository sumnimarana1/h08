package com.spnotes.kafka.simple;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.Scanner;

/**
 * Created by sunilpatil on 12/28/15.
 */
public class Producer {
    private static Scanner in;
    private static int a;
    public static void main(String[] argv)throws Exception {
        if (argv.length != 1) {
            System.err.println("Please specify 1 parameters ");
            System.exit(-1);
        }
        String topicName = argv[0];
        in = new Scanner(System.in);
        // System.out.println("Enter message(type exit to quit)");

        //Configure the Producer
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.ByteArraySerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");
        org.apache.kafka.clients.producer.Producer producer = new KafkaProducer(configProperties);
//        Random random = new Random();
            a=0;
            System.out.println("Enter the flavor of Icecream:");
            System.out.println("***********************");
            System.out.println("Enter a number between 1 and 4: ");
            a = in.nextInt();
           
            String[] sentences = new String[] {
               
                "Strawberry",
                "Chocolate chip",
                "Vanilla",
                "Double Chocolate",

            };
//        String line = in.nextLine();
//        for(int i = 0; i < 20; i++) {
            // Pick a sentence at random
            //String s = sentences[random.nextInt(sentences.length)];
            // Send the sentence to the test topic

            String s = "Ice Cream Flavor!!\n"+"******************\n"+ sentences[a-1];
            ProducerRecord<String, String> rec = new ProducerRecord<String, String>(topicName, s);
            producer.send(rec);
            s = in.nextLine();
//        }
        in.close();
        producer.close();
    }
}
